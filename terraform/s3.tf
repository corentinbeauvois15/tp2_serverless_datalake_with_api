# TODO : Create a s3 bucket with aws_s3_bucket

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-benjamin2020corentin"
  acl    = "private"
  force_destroy = true

  tags = {
    Name        = "s3-job-offer-bucket-benjamin2020corentin"
    Environment = "Dev"
  }

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_object" "raw" {
  bucket = aws_s3_bucket.b.id
  key    = "job_offer/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.b.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "AWSLogs/"
    filter_suffix       = ".log"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}