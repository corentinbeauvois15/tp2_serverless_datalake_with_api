# TODO : Create the lambda role with aws_iam_role

# TODO : Create lambda function with aws_lambda_function

# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role

# TODO : Attach the logging policy to the lambda role with aws_iam_role_policy_attachment

# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment

# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "test_lambda" {
  filename      = "empty_lambda_code.zip"
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "lambda_main_app.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  //source_code_hash = filebase64sha256("lambda_function_payload.zip")

  runtime = "python3.7"

  environment {
    variables = {
      foo = "bar"
    }
  }
}

# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.b.arn
}